package com.mobile.randomcityapp.ui.list

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.mobile.randomcityapp.R
import java.text.SimpleDateFormat

class CityAdapter(private val context: Context, private val clickListener: CityClickListener) : RecyclerView.Adapter<CityAdapter.ViewHolder>() {

    var data = listOf<CityDetails>()

    @SuppressLint("SimpleDateFormat")
    val timeFormatter = SimpleDateFormat("hh:mm:ss")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_city, parent, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.city.text = item.city.name
        holder.city.setTextColor(ContextCompat.getColor(context, item.color.value))
        holder.timestamp.text = timeFormatter.format(item.timestamp)
        holder.itemView.setOnClickListener { clickListener.onCityClicked(item) }
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val city: TextView = itemView.findViewById<TextView>(R.id.city)
        val timestamp: TextView = itemView.findViewById<TextView>(R.id.timestamp)
    }
}

interface CityClickListener {
    fun onCityClicked(cityDetails: CityDetails)
}