package com.mobile.randomcityapp.ui.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mobile.randomcityapp.data.*
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import java.io.Serializable

class ListViewModel(private val dataSource: DataSource) : ViewModel() {
    val data = MutableLiveData<List<CityDetails>>()

    var disposable: Disposable? = null

    private val cityMapper = CityMapper()
    private val colorMapper = ColorMapper()

    fun startEmission() {
        if (disposable == null || disposable?.isDisposed == true) {
            data.value = dataSource.emittedItems.map { mapData(it) }
            disposable = dataSource.newItems
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { updateList(it) }
        }
    }

    private fun updateList(value: EmittedData) {
        val list = mutableListOf<CityDetails>()
        data.value?.let { list.addAll(it) }
        list.add(mapData(value))
        data.value = list
    }

    private fun mapData(emittedData: EmittedData): CityDetails {
        return CityDetails(city = cityMapper.invoke(emittedData.city), color = colorMapper.invoke(emittedData.color), timestamp = emittedData.timestamp)
    }

    override fun onCleared() {
        disposable?.dispose()
        super.onCleared()
    }

    fun restartEmission() {
        startEmission()
        dataSource.startEmission()
    }

    fun stopEmission() {
        disposable?.dispose()
        dataSource.stopEmission()
    }

    fun getEmittedItems(): List<EmittedData> {
        return dataSource.emittedItems
    }
}


data class CityDetails(val city: City, val color: Color, val timestamp: Long) : Serializable