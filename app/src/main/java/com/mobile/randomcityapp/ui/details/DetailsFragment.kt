package com.mobile.randomcityapp.ui.details

import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mobile.randomcityapp.R
import com.mobile.randomcityapp.ui.list.CityDetails
import kotlinx.android.synthetic.main.fragment_details.*
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint


class DetailsFragment : Fragment() {

    private val cityDetails: CityDetails by lazy { arguments?.getSerializable(CITY) as? CityDetails ?: throw IllegalStateException("City cannot be null") }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val applicationContext = activity?.applicationContext
        Configuration.getInstance().load(requireContext(), PreferenceManager.getDefaultSharedPreferences(applicationContext))
        map.setTileSource(TileSourceFactory.MAPNIK)
    }

    override fun onResume() {
        super.onResume()
        map.onResume()

        map.controller.setZoom(12.0)
        map.controller.setCenter( GeoPoint(cityDetails.city.latitude, cityDetails.city.longitude))
    }

    override fun onPause() {
        super.onPause()
        map.onPause()
    }

    companion object {
        private const val CITY = "city_arg_fragment"

        fun newInstance(city: CityDetails): DetailsFragment {
            return DetailsFragment().apply { arguments = Bundle().apply { this.putSerializable(CITY, city) } }
        }
    }
}