package com.mobile.randomcityapp.ui.main

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.mobile.randomcityapp.R
import com.mobile.randomcityapp.data.EmittedData
import com.mobile.randomcityapp.ui.details.DetailsActivity
import com.mobile.randomcityapp.ui.details.DetailsFragment
import com.mobile.randomcityapp.ui.list.CityDetails
import com.mobile.randomcityapp.utlis.FileHelper
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.java.KoinJavaComponent.get

class MainActivity : AppCompatActivity(), RandomCityEvents {

    private val fileHelper: FileHelper = get(FileHelper::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkPermissions()
    }

    private fun checkPermissions() {
        if (!isWritePermissionGranted()) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_WRITE_PERMISSIONS)
        }
    }

    private fun isWritePermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == REQUEST_WRITE_PERMISSIONS) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission not granted. The app may not work properly", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onCityClicked(cityDetails: CityDetails) {
        if (details != null) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.details, DetailsFragment.newInstance(cityDetails))
                .commit()
        } else {
            DetailsActivity.start(this, cityDetails)
        }
    }

    override fun onSaveData(list: List<EmittedData>) {
        if (isWritePermissionGranted()) {
            fileHelper.save(list)
        }
    }

    companion object {
        const val REQUEST_WRITE_PERMISSIONS = 1234

        fun start(context: Context) {
            context.startActivity(Intent(context, MainActivity::class.java))
        }
    }
}

interface RandomCityEvents {
    fun onCityClicked(cityDetails: CityDetails)

    fun onSaveData(list: List<EmittedData>)
}