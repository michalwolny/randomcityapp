package com.mobile.randomcityapp.ui.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.mobile.randomcityapp.R
import com.mobile.randomcityapp.ui.list.CityDetails
import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        val cityDetails = intent?.extras?.getSerializable(CITY) as? CityDetails ?: throw IllegalStateException("Received city cannot be null")

        toolbar.title = cityDetails.city.name
        toolbar.setBackgroundColor(ContextCompat.getColor(this, cityDetails.color.value))
        toolbar.setTitleTextColor(ContextCompat.getColor(this, android.R.color.darker_gray))
        setSupportActionBar(toolbar)

        supportFragmentManager
            .beginTransaction()
            .add(R.id.details, DetailsFragment.newInstance(cityDetails))
            .commit()
    }

    companion object {
        private const val CITY = "city_arg"
        fun start(context: Context, cityDetails: CityDetails) {
            val intent = Intent(context, DetailsActivity::class.java)
            val bundle = Bundle()
            bundle.putSerializable(CITY, cityDetails)
            intent.putExtras(bundle)
            context.startActivity(intent)
        }
    }
}