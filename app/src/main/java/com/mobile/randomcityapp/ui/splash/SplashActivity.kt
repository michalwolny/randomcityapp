package com.mobile.randomcityapp.ui.splash

import android.app.Activity
import android.os.Bundle
import com.mobile.randomcityapp.R
import com.mobile.randomcityapp.data.DataSource
import com.mobile.randomcityapp.ui.main.MainActivity
import io.reactivex.rxjava3.disposables.Disposable
import org.koin.java.KoinJavaComponent.get

class SplashActivity : Activity() {

    private val dataSource: DataSource = get(DataSource::class.java)

    private var disposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        disposable = dataSource.newItems.subscribe { onFirstItemEmitted() }
        dataSource.startEmission()
    }

    private fun onFirstItemEmitted() {
        disposable?.dispose()
        MainActivity.start(this)
        finish()
    }
}