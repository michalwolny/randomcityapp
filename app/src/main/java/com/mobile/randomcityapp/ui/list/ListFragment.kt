package com.mobile.randomcityapp.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.mobile.randomcityapp.LifecycleListener
import com.mobile.randomcityapp.R
import com.mobile.randomcityapp.RandomCityApp
import com.mobile.randomcityapp.ui.main.RandomCityEvents
import kotlinx.android.synthetic.main.fragment_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ListFragment : Fragment(), CityClickListener, LifecycleListener {

    private val viewModel: ListViewModel by viewModel()
    private val adapter: CityAdapter by lazy { CityAdapter(requireContext(), this) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = LinearLayoutManager(requireContext())
        cities_recycler_view.adapter = adapter
        cities_recycler_view.layoutManager = layoutManager
        cities_recycler_view.addItemDecoration(DividerItemDecoration(requireContext(), layoutManager.orientation))

        viewModel.startEmission()
        viewModel.data.observe(viewLifecycleOwner, Observer { state ->
            adapter.data = state.sortedBy { cityDetails -> cityDetails.city.name }
            adapter.notifyDataSetChanged()
        })

        (activity?.application as? RandomCityApp)?.lifecycleListener = this
    }

    override fun onForeground() {
        viewModel.restartEmission()
    }

    override fun onBackground() {
        viewModel.stopEmission()
        (activity as? RandomCityEvents)?.onSaveData(viewModel.getEmittedItems())
    }

    override fun onCityClicked(cityDetails: CityDetails) {
        (activity as? RandomCityEvents)?.onCityClicked(cityDetails)
    }

}