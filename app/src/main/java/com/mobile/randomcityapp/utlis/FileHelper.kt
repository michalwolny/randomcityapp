package com.mobile.randomcityapp.utlis

import android.annotation.SuppressLint
import android.content.Context
import android.os.Environment
import android.util.Log
import android.widget.Toast
import com.mobile.randomcityapp.data.EmittedData
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat

class FileHelper(private val context: Context) {
    @SuppressLint("SimpleDateFormat")
    private val dateFormatter = SimpleDateFormat("dd_MM_yyyy_HH_mm_ss")

    @SuppressLint("SimpleDateFormat")
    private val timeFormatter = SimpleDateFormat("dd/MM HH:mm:ss")

    fun save(list: List<EmittedData>) : Boolean {
        var success = true
        val builder = StringBuilder()
        list.forEach { builder.append("${timeFormatter.format(it.timestamp)} ${it.city} ${it.color}\n") }
        val data = builder.toString()

        if (Environment.MEDIA_MOUNTED != Environment.getExternalStorageState()) return false
        var outputStream: FileOutputStream? = null

        try {
            val dir = File(Environment.getExternalStorageDirectory().absolutePath, Environment.DIRECTORY_DOWNLOADS)
            if (!dir.exists()) dir.mkdir()

            val file = File(dir, getFileName())
            file.createNewFile()
            outputStream = FileOutputStream(file)
            outputStream.write(data.toByteArray())
            outputStream.flush()

            Toast.makeText(context, "Data successfully saved to a file", Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            Log.e("FileHelper", e.toString())
            success = false
        } finally {
            outputStream?.close()
            return success
        }
    }

    private fun getFileName() = "cities_${dateFormatter.format(System.currentTimeMillis())}.txt"
}