package com.mobile.randomcityapp.di

import com.mobile.randomcityapp.data.DataSource
import com.mobile.randomcityapp.ui.list.ListViewModel
import com.mobile.randomcityapp.utlis.FileHelper
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val viewModule: Module = module {
    viewModel { ListViewModel(get()) }
}

val dataModule: Module = module {
    single { DataSource() }
}

val utilsModule: Module = module {
    single { FileHelper(get()) }
}
