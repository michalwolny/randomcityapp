package com.mobile.randomcityapp

import android.app.Application
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import com.mobile.randomcityapp.di.dataModule
import com.mobile.randomcityapp.di.utilsModule
import com.mobile.randomcityapp.di.viewModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class RandomCityApp : Application(), LifecycleObserver {

    var lifecycleListener: LifecycleListener? = null

    override fun onCreate() {
        super.onCreate()
        initKoin()

        ProcessLifecycleOwner
            .get()
            .lifecycle
            .addObserver(this)
    }

    private fun initKoin() = startKoin {
        androidContext(this@RandomCityApp)
        modules(viewModule, dataModule, utilsModule)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
        lifecycleListener?.onBackground()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppForegrounded() {
        lifecycleListener?.onForeground()
    }
}

interface LifecycleListener {
    fun onForeground()

    fun onBackground()
}