package com.mobile.randomcityapp.data

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.plusAssign
import io.reactivex.rxjava3.subjects.PublishSubject
import java.util.concurrent.TimeUnit

val cities = listOf("Gdańsk", "Warszawa", "Poznań", "Białystok", "Wrocław", "Katowice", "Kraków")
val colors = listOf("Yellow", "Green", "Blue", "Red", "Black", "White")

data class EmittedData(val city: String, val color: String, val timestamp: Long)

class DataSource {
    private var disposable: CompositeDisposable = CompositeDisposable()
    val emittedItems = mutableListOf<EmittedData>()
    val newItems: PublishSubject<EmittedData> = PublishSubject.create()

    fun startEmission() {
        disposable = CompositeDisposable()
        disposable += Observable
            .interval(3, 5, TimeUnit.SECONDS)
            .map { EmittedData(cities.random(), colors.random(), System.currentTimeMillis()) }
            .subscribe {
                emittedItems.add(it)
                newItems.onNext(it)
            }
    }

    fun stopEmission() {
        disposable.dispose()
    }
}