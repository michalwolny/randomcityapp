package com.mobile.randomcityapp.data

import java.io.Serializable

sealed class City(val name: String, val latitude: Double, val longitude: Double) : Serializable
object Gdansk : City("Gdańsk", 54.3483, 18.6535)
object Warsaw : City("Warszawa", 52.2493, 21.012)
object Poznan : City("Poznań", 52.40824, 16.93345)
object Bialystok : City("Białystok", 53.1323, 23.1588)
object Wroclaw : City("Wrocław", 51.1098, 17.0322)
object Katowice : City("Katowice", 50.2597, 19.0218)
object Krakow : City("Kraków", 50.06186, 19.93687)


class CityMapper() {
    operator fun invoke(name: String): City {
        return when (name) {
            Gdansk.name -> Gdansk
            Warsaw.name -> Warsaw
            Poznan.name -> Poznan
            Bialystok.name -> Bialystok
            Wroclaw.name -> Wroclaw
            Katowice.name -> Katowice
            Krakow.name -> Krakow
            else -> throw IllegalStateException("Unknown city: $name")
        }
    }
}

