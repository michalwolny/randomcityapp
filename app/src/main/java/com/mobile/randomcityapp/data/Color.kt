package com.mobile.randomcityapp.data

import androidx.annotation.ColorRes
import com.mobile.randomcityapp.R
import java.io.Serializable

sealed class Color(@ColorRes val value: Int): Serializable
object  Yellow: Color(R.color.yellow)
object  Green: Color(R.color.green)
object  Blue: Color(R.color.blue)
object  Red: Color(R.color.red)
object  Black: Color(R.color.black)
object  White: Color(R.color.white)

class ColorMapper() {
    operator fun invoke(name: String) : Color {
        return when(name) {
            "Yellow" -> Yellow
            "Green"-> Green
            "Blue"-> Blue
            "Red" -> Red
            "Black"-> Black
            "White"-> White
            else -> throw IllegalStateException("Unknown color: $name")}
    }
}