package com.mobile.randomcityapp.data

import org.junit.Assert
import org.junit.Before
import org.junit.Test

internal class CityMapperTest {

    lateinit var mapper: CityMapper

    @Before
    fun setUp() {
        mapper = CityMapper()
    }

    @Test
    fun `check if city name correctly mapped to object`() {
        Assert.assertEquals(Wroclaw, mapper("Wrocław"))
        Assert.assertEquals(Warsaw, mapper("Warszawa"))
        Assert.assertEquals(Poznan, mapper("Poznań"))
        Assert.assertEquals(Gdansk, mapper("Gdańsk"))
        Assert.assertEquals(Krakow, mapper("Kraków"))
        Assert.assertEquals(Katowice, mapper("Katowice"))
        Assert.assertEquals(Bialystok, mapper("Białystok"))
    }

    @Test(expected = IllegalStateException::class)
    fun `check if proper exception thrown when unknown city name`() {
        Assert.assertEquals(Wroclaw, mapper("Praga"))
    }

    @Test(expected = IllegalStateException::class)
    fun `check if proper exception thrown when a typo in city name`() {
        Assert.assertEquals(Wroclaw, mapper("Wroclaw"))
    }
}