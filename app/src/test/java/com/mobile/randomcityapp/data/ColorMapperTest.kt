package com.mobile.randomcityapp.data

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

internal class ColorMapperTest {

    lateinit var mapper: ColorMapper

    @Before
    fun setUp() {
        mapper = ColorMapper()
    }

    @Test
    fun `check if color name correctly mapped to object`() {
        assertEquals(Green, mapper("Green"))
        assertEquals(Yellow, mapper("Yellow"))
        assertEquals(Blue, mapper("Blue"))
        assertEquals(Red, mapper("Red"))
        assertEquals(White, mapper("White"))
        assertEquals(Black, mapper("Black"))
    }

    @Test(expected = IllegalStateException::class)
    fun `check if proper exception thrown when unknown color name`() {
        assertEquals(Black, mapper("Pink"))
    }
}